# moncon

Script using `ddcutil` for switch monitor input sources.

## Setup

```sh
sudo apt install ddcutil

# Or whatever is in your $PATH
ln -s ~/<path>/moncon/mon-play ~/.local/bin/mon-play
ln -s ~/<path>/moncon/mon-work ~/.local/bin/mon-work

ln -s ~/<path>/moncon/dark-day ~/.local/bin/dark-day
ln -s ~/<path>/moncon/bright-day ~/.local/bin/bright-day

ln -s ~/<path>/moncon/export-bus-numbers ~/.local/bin/export-bus-numbers
```

Include the following in `~/.bashrc`:
```sh
# Export monitor bus numbers for ddcutil
BUS_EXPORTS=$HOME/.local/bin/export-bus-numbers
if [[ -f "$BUS_EXPORTS" ]]; then
  source $BUS_EXPORTS
fi
```

## Updates

```sh
# Detect display. This will include a display number as well as a bus number.
#  e.g. "/dev/i2c-4", where 4 is the bus number
#  The --brief option can be specified for a shorter output.
sudo ddcutil detect

# View commands and features of display <n>
sudo ddcutil -d <n> capabilities
```

If monitor i2c bus numbers change, this file will need updates as well:
```sh
# Update the `KERNEL=="i2c-XX"` value
sudo vim /etc/udev/rules.d/98-ddc.rules

# Restart udev afterwards
sudo udevadm control --reload-rules && sudo udevadm trigger
```

## Tricks

Specifying a display via the `--bus` argument results in a much faster run since `ddcutil` does not
need to scan every `i2c` device first.
